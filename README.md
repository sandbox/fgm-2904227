# Reinstall

## Usage notes

### Support for term hierarchy

This mechanism supports multiple parents on terms. However, it needs the 
migrations to include its callback, within a process pipeline like:

```yaml
# In migrate_plus.migration.reinstall_terms_foo
process:
  parent_id:
    -
      plugin: skip_on_empty
      method: process
      source: parent
    -
      # This step is needed to transform the terms parents to usable values.
      # Without it, all terms will be at level 0 in the target vocabulary.
      plugin: callback
      callable:
        - '\Drupal\reinstall\Plugin\migrate\source\ReinstallTermSource'
        - 'getParent'
    -
      plugin: migration
      # Notice the reference to the current migration.
      migration: reinstall_terms_foo
  parent:
    plugin: default_value
    default_value: 0
    source: '@parent_id'
```
