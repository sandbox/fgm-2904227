<?php

/**
 * @file
 * Export content into YAML files.
 */

/**
 * Implements hook_drush_command().
 */
function reinstall_drush_command() {
  $items['reinstall-export'] = [
    'description' => 'Export site content.',
    'arguments' => [],
    'required-arguments' => 0,
    'aliases' => ['rex'],
    'options' => [],
    'drupal dependencies' => [],
  ];

  return $items;
}

/**
 * Drush callback for reinstall-export.
 *
 * @param string|null $entity_type
 *   The entity type.
 * @param array $bundles
 *   An array of bundle names to export.
 */
function drush_reinstall_export(?string $entity_type = NULL, array ...$bundles) {

  /** @var \Drupal\reinstall\Dumper $dumper */
  $dumper = \Drupal::service('reinstall.dumper');
  $dumper->dump($entity_type, $bundles);
}
