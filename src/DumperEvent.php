<?php

namespace Drupal\reinstall;

use Drupal\Core\Entity\EntityStorageInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Wraps a dump event for event listeners.
 *
 * @see \Drupal\reinstall\ReinstallEvents::REINSTALL_SERIALIZE_POST
 * @see \Drupal\reinstall\ReinstallEvents::SERIALIZE_PRE
 */
class DumperEvent extends Event {

  /**
   * The name of the bundle for which a dump is being performed.
   *
   * @var string
   */
  public $bundleName;

  /**
   * The entities to dump.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  public $entities = [];

  /**
   * The storage handler for the entity type of the entities to dump.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  public $storage;

  /**
   * DumperEvent constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage.
   * @param string $bundleName
   *   The name of the bundle being dumped.
   * @param array $entities
   *   The entities to dump.
   */
  public function __construct(EntityStorageInterface $storage, string $bundleName, array $entities) {
    $this->storage = $storage;
    $this->bundleName = $bundleName;
    $this->entities = $entities;
  }

}
