<?php

namespace Drupal\reinstall\Plugin\migrate\source;

/**
 * Class SimpleSourceTrait provides shared features for entity field handling.
 */
trait SimpleSourceTrait {

  /**
   * The entity_type.bundle.info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $sstEntityTypeBundleInfo;

  /**
   * The entity_field.manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $sstEntityFieldManager;

  /**
   * The name of the entity type being handled.
   *
   * @var string
   */
  protected $sstEntityType;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $sstEntityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $bundles = $this->getEntityTypeBundleInfo()
      ->getBundleInfo($typeName = $this->getEntityType());
    $rows = [];

    foreach ($bundles as $bundleName => $bundle) {
      $fieldDefinitions = $this->getEntityFieldManager()
        ->getFieldDefinitions($typeName, $bundleName);
      foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
        $rows[$fieldName][$bundleName] = $fieldDefinition->getLabel();
      }
    }

    $fields = [];
    $singleBundle = count($bundles) === 1;
    foreach ($rows as $fieldName => $labels) {
      if ($singleBundle) {
        $fields[$fieldName] = reset($labels);
        continue;
      }

      if (count(array_unique($labels)) === 1) {
        $fields[$fieldName] = reset($labels);
        continue;
      }

      $ret = [];
      ksort($labels);
      foreach ($labels as $ct => $label) {
        $ret[] = $this->t('@ct: @label', ['@ct' => $ct, '@label' => $label]);
      }
      $fields[$fieldName] = implode(', ', $ret);
    }

    ksort($fields);
    return $fields;
  }

  /**
   * Getter for the entity_type.bundle.info service.
   *
   * @return \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   *   The service.
   */
  protected function getEntityTypeBundleInfo() {
    if (!isset($this->sstEntityTypeBundleInfo)) {
      $this->sstEntityTypeBundleInfo = \Drupal::service('entity_type.bundle.info');
    }

    return $this->sstEntityTypeBundleInfo;
  }

  /**
   * Getter for the entity_field.manager service.
   *
   * @return \Drupal\Core\Entity\EntityFieldManagerInterface|mixed
   *   The service.
   */
  protected function getEntityFieldManager() {
    if (!isset($this->sstEntityFieldManager)) {
      $this->sstEntityFieldManager = \Drupal::service('entity_field.manager');
    }

    return $this->sstEntityFieldManager;
  }

  /**
   * Getter for the current entity type.
   *
   * @return string
   *   The machine name of the type.
   */
  protected function getEntityType() {
    assert(isset($this->sstEntityType));
    return $this->sstEntityType;
  }

  /**
   * Getter for the entity_type.manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface|mixed
   *   The service.
   */
  protected function getEntityTypeManager() {
    if (!isset($this->sstEntityTypeManager)) {
      $this->sstEntityTypeManager = \Drupal::service('entity_type.manager');
    }

    return $this->sstEntityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $typeName = $this->getEntityType();

    $typeDefinition = $this->getEntityTypeManager()->getDefinition($typeName);
    $idName = $typeDefinition->getKey('id');
    assert(!empty($idName));

    $definitions = $this->getEntityFieldManager()->getBaseFieldDefinitions($typeName);
    assert(isset($definitions[$idName]));
    $idDefinition = $definitions[$idName];
    $idType = $idDefinition->getType();
    $ids = [
      $idName => [
        'type' => $idType,
      ],
    ];

    return $ids;
  }

}
