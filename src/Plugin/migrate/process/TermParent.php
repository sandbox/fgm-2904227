<?php

namespace Drupal\reinstall\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class TermParent works in conjunction with ReinstallTermSource.
 *
 * With the latter using the heavy-handed SimpleSource::flattenRow() method,
 * the parents obtained when migrating hierarchical vocabularies can be wrong,
 * especially with terms having multiple parents (i.e. non-tree graphs), and
 * this method allows the migration to obtain proper parent target_id values.
 *
 * @MigrateProcessPlugin(
 *   id = "reinstall_term_parents"
 * )
 */
class TermParent extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform(
    $value,
    MigrateExecutableInterface $migrate_executable,
    Row $row,
    $destination_property
  ) {
    if (is_scalar($value)) {
      return $value;
    }
    elseif (isset($value['target_id'])) {
      return $value['target_id'];
    }
  }

}
