<?php

namespace Drupal\reinstall\Commands;

use Drupal\reinstall\Dumper;
use Drush\Commands\DrushCommands;

/**
 * ReinstallCommands provides the reinstall:export command.
 */
class ReinstallCommands extends DrushCommands {

  /**
   * The reinstall.dumper service.
   *
   * @var \Drupal\reinstall\Dumper
   */
  protected $dumper;

  /**
   * ReinstallCommands constructor.
   *
   * @param \Drupal\reinstall\Dumper $dumper
   *   The reinstall.dumper service.
   */
  public function __construct(Dumper $dumper) {
    $this->dumper = $dumper;
  }

  /**
   * Export site content.
   *
   * @param string|null $entity_type
   *   Optional. A single entity type. If absent, export all bundles of all
   *   entity types.
   * @param array|null $bundles
   *   Optional. An array of bundle names to export. If $entity_type is not set,
   *   it must not be set either.
   *
   * @command reinstall:export
   * @aliases rex,reinstall-export
   */
  public function export(?string $entity_type = NULL, ?string $bundles = NULL) {
    if (empty($entity_type) && !empty($bundles)) {
      throw new \InvalidArgumentException("Bundles may not be set if entity_type is not.");
    }
    $arBundles = array_filter(explode(',', $bundles));
    $this->dumper->dump($entity_type, $arBundles);
  }

}
