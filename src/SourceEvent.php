<?php

namespace Drupal\reinstall;

use Drupal\reinstall\Plugin\migrate\source\ReinstallSourceBase;
use Symfony\Component\EventDispatcher\Event;

/**
 * And event being triggered during dumps.
 *
 * @see \Drupal\reinstall\ReinstallEvents::POST_SOURCE_PARSE
 */
class SourceEvent extends Event {

  /**
   * The source for the migration.
   *
   * @var \Drupal\migrate\Plugin\MigrateSourceInterface
   */
  public $source;

  /**
   * DumperEvent constructor.
   *
   * @param \Drupal\reinstall\Plugin\migrate\source\ReinstallSourceBase $source
   *   The data source.
   */
  public function __construct(ReinstallSourceBase $source) {
    $this->source = $source;
  }

}
