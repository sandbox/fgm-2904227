<?php

namespace Drupal\reinstall;

/**
 * Defines events for the Dumper system.
 *
 * @see \Drupal\reinstall\Dumper
 */
final class ReinstallEvents {

  /**
   * Name of the event fired when entities have been dumped.
   *
   * This event allows modules to perform an action whenever content entities
   * of a given entity type bundle are exported, usually to perform extra
   * operations, like:
   * - copying/moving the actual files on file export.
   *
   * The event listener method receives a \Drupal\reinstall\SerializeEvent
   * instance, carrying an array of all entites indexed by $entity->id().
   *
   * Note that this assumes entity types with an ID, which is not stricly
   * required by the Entity API, but common enough for most needs.
   *
   * @Event
   *
   * @var string
   */
  const POST_DUMP = 'reinstall.dump.post';

  /**
   * Name of the event fired when readying entities for dump.
   *
   * This event allows modules to perform an action whenever content entities
   * of a given entity type bundle are exported, usually to add data to the
   * export, like:
   * - parent on taxonomy_term.
   * - path on any entity with a canonical path.
   *
   * The event listener method receives a \Drupal\reinstall\DumperEvent
   * instance, carrying an array of all entites indexed by $entity->id().
   *
   * Note that this assumes entity types with an ID, which is not stricly
   * required by the Entity API, but common enough for most needs.
   *
   * @Event
   *
   * @var string
   */
  const PRE_DUMP = 'reinstall.dump.pre';

  const POST_SOURCE_PARSE = 'reinstall.source_parse.post';

}
