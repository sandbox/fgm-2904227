<?php

namespace Drupal\reinstall\EventSubscriber;

use Drupal\Core\File\Exception\FileNotExistsException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\reinstall\DumperEvent;
use Drupal\reinstall\ReinstallEvents;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class FilePostDump performs physical file copy on file dump.
 */
class FilePostDump implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The file_system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The export/import path.
   *
   * @var string
   */
  protected $importPath;

  /**
   * The stream_wrapper_manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The logger.channel.reinstall service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * FilePostDump constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file_system service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $wrapperManager
   *   The stream_wrapper_manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.channel.reinstall service.
   * @param string $importPath
   *   The reinstall.path parameter.
   */
  public function __construct(
    FileSystemInterface $fileSystem,
    StreamWrapperManagerInterface $wrapperManager,
    LoggerInterface $logger,
    string $importPath
  ) {
    $this->fileSystem = $fileSystem;
    $this->logger = $logger;
    $this->streamWrapperManager = $wrapperManager;

    $this->importPath = $importPath;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ReinstallEvents::POST_DUMP => 'onDumpPost',
    ];
  }

  /**
   * An array_reduce() callback to collect namespaces from file entities.
   *
   * @param string[] $accu
   *   The namespaces accumulator.
   * @param \Drupal\file\Entity\File $fileItem
   *   A file description.
   *
   * @return string[]
   *   An array of namespaces used on the site.
   *
   * @see \Drupal\reinstall\Dumper::dumpFiles()
   */
  protected static function namespacesReducer(array $accu, File $fileItem) {
    $uri = $fileItem->getFileUri();
    // Plain filenames without a namespace. Should not happen, but...
    if (FALSE === ($len = mb_strpos($uri, '://'))) {
      return $accu;
    };

    $namespace = mb_substr($uri, 0, $len);
    $accu[$namespace] = TRUE;
    return $accu;
  }

  /**
   * Callback for POST_DUMP event.
   *
   * @param \Drupal\reinstall\DumperEvent $event
   *   The post-dump event.
   */
  public function onDumpPost(DumperEvent $event) {
    if ($event->storage->getEntityTypeId() !== 'file') {
      return;
    }

    $this->dumpFiles($event->bundleName, $event->entities);
  }

  /**
   * Dump the actual files for a file entity bundle.
   *
   * @param string $bundleName
   *   The bundle name.
   * @param array $files
   *   The file URLs.
   */
  public function dumpFiles(string $bundleName, array $files) {
    $this->logger->debug('Dumping file/@bundle', [
      '@bundle' => $bundleName,
    ]);
    $importPath = $this->importPath;
    $dir = "$importPath/$bundleName";
    $usedNamespaces = array_keys(array_reduce($files,
      [__CLASS__, 'namespacesReducer'],
      []
    ));
    $lists = [];

    foreach ($usedNamespaces as $ns) {
      // XXX Consider using \0 to support xargs: file names MAY contain spaces.
      $path = "$dir/$ns.list.txt";

      $nsDir = "$dir/$ns";
      if (!is_dir($nsDir)) {
        echo "Creating $nsDir\n";
        mkdir($nsDir, 0777, TRUE);
      }

      // fopen() is in text mode by default.
      $lists[$ns] = [
        'dir' => $nsDir,
        'handle' => fopen($path, 'w'),
      ];
    }
    $skipped = 0;

    /** @var \Drupal\file\Entity\File $file */
    foreach ($files as $file) {
      $uri = $file->getFileUri();
      $target = StreamWrapperManager::getTarget($uri);
      $ns = StreamWrapperManager::getScheme($uri);
      fwrite($lists[$ns]['handle'], $target . "\n");
      $dest = $lists[$ns]['dir'] . '/' . $target;

      $dir = dirname($dest);
      if (!is_dir($dir)) {
        mkdir($dir, 0777, TRUE);
      }
      try {
        $this->fileSystem->copy($uri, $dest, FileSystemInterface::EXISTS_REPLACE);
      }
      catch (FileNotExistsException $e) {
        $skipped++;
      }
      catch (\Exception $e) {
        $this->logger->warning($e->getMessage());
      }
    }
    if (!empty($skipped)) {
      $this->logger->warning($this->formatPlural(
        $skipped,
        "Failed 1 copy for bundle file/@bundle.",
        "Failed @count copies for bundle file/@bundle", [
          '@bundle' => $bundleName,
        ]
      ));
    }

    foreach ($lists as $list) {
      fclose($list['handle']);
    }
  }

}
