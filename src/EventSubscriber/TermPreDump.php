<?php

namespace Drupal\reinstall\EventSubscriber;

use Drupal\reinstall\DumperEvent;
use Drupal\reinstall\ReinstallEvents;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class TermPreDump adds the parents target_id values on a term.
 */
class TermPreDump implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ReinstallEvents::PRE_DUMP => 'onDumpPre',
    ];
  }

  /**
   * Set the parents on the term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to complete.
   * @param int $tid
   *   The term id.
   * @param \Drupal\taxonomy\TermStorageInterface $storage
   *   The term storage.
   */
  protected static function setParents(TermInterface $term, $tid, TermStorageInterface $storage) {
    $parents = $storage->loadParents($term->id());
    if (!empty($parents)) {
      $term->set('parent', array_keys($parents));
    }
  }

  /**
   * Callback for PRE_DUMP event.
   *
   * @param \Drupal\reinstall\DumperEvent $event
   *   The pre-dump event.
   */
  public static function onDumpPre(DumperEvent $event) {
    $storage = $event->storage;
    if ($storage->getEntityTypeId() !== 'taxonomy_term') {
      return;
    }

    array_walk($event->entities, [__CLASS__, 'setParents'], $storage);
  }

}
